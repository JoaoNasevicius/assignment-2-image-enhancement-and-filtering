# Assignment 2 -  Image Enhancement and Filtering

Image Processing - scc0251. 

Authors:
* João Vítor Nasevicius Ramos - Nº USP 9894540


Folders and files:
* [Python code](./submissions/dip02.py) contains the Python code used for run.codes submission
* [Images](/images) contains images used in the demos
* [Notebook with Demo](dip02.ipynb) is a notebook exemplifying functions developed and submitted