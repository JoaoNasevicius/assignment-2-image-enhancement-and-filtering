"""
Joao Vitor Nasevicius Ramos / 9894540
SCC 0251
2020.1

Assignment 2: Image Enhancement and Filtering
"""


import numpy as np
import imageio
import math

def BilateralFilter(input_img, n, spatialSigma, rangeSigma):
    num_row, num_col = input_img.shape
    mid = n // 2

    output_img = np.zeros((num_col, num_row), dtype= int)
    
    spatialComponent = SpatialGaussianComponent(n, spatialSigma)

    for x in range(0, num_col):
        for y in range(0, num_row):

            inicialIntensityMatrix = np.zeros((n, n), dtype= np.float64)        #Para facilitar os calculos, a intensidade da regiao e' armazenada em uma matrix
            for x_filter in range(0, n):
                for y_filter in range(0, n):
                    if x + x_filter - mid >= 0 and x + x_filter - mid < num_col and y + y_filter - mid >= 0 and y + y_filter - mid < num_row:       #Garante que o pixel esteja entre 0 e a largura/comprimento da image
                        inicialIntensityMatrix[x_filter,y_filter] = input_img[x + x_filter - mid,y + y_filter - mid] 

            rangeComponent = RangeGaussianComponent(inicialIntensityMatrix, n, rangeSigma)


            wi = spatialComponent * rangeComponent              #Multiplicamos os elementos, um a um, das matrizes para termos os respectivos "Wi"s
            wf = np.sum(wi)

            finalIntensityMatrix = inicialIntensityMatrix * wi  #Multiplicamos os elemntos, um a um, para termos os valores da Ii multiplicado pelo seu respectivo Wi
            finalIntensity = np.sum(finalIntensityMatrix)
            finalIntensity = finalIntensity//wf

            output_img[x,y] = finalIntensity

    return output_img



def LaplacianFilter(input_img, kernel, c):
    num_row, num_col = input_img.shape
    mid = 1

    output_img = np.zeros((num_col, num_row), dtype= np.float64)

    if kernel == 1:
        kernel = np.array([[ 0, -1,  0],
                           [-1,  4, -1],
                           [ 0, -1,  0]])
    elif kernel == 2:
        kernel = np.array([[-1, -1, -1],
                           [-1,  8, -1],
                           [-1, -1, -1]])

    for x in range(0, num_col):
        for y in range(0, num_row):

            intensityMatrix = np.zeros((3, 3), dtype= np.float64)
            for x_filter in range(0, 3):
                for y_filter in range(0, 3):
                    if x + x_filter - mid >= 0 and x + x_filter - mid < num_col and y + y_filter - mid >= 0 and y + y_filter - mid < num_row:
                        intensityMatrix[x_filter,y_filter] = input_img[x + x_filter - mid,y + y_filter - mid]

            intensityMatrix = intensityMatrix * kernel
            output_img[x, y] = np.sum(intensityMatrix)

    output_img = Scaling(output_img)
    output_img = input_img + (output_img * c)
    output_img = Scaling(output_img)

    return output_img



def VignetteFilter(input_img, rowSigma, colSigma):
    num_row, num_col = input_img.shape

    output_img = np.zeros((num_col, num_row), dtype= np.float64)

    centerRow = num_row // 2 - (1 - (num_row % 2))
    centerCol = num_col // 2 - (1 - (num_col % 2))

    wRow = np.zeros((num_row, 1), dtype= np.float64)
    wCol = np.zeros((1, num_col), dtype= np.float64)

    for row in range(0, num_row):
        wRow[row, 0] = row - centerRow 
    for col in range(0, num_col):
        wCol[0, col] = col - centerCol

    wRow = GaussianKernel(wRow, rowSigma)
    wCol = GaussianKernel(wCol, colSigma)

    w = np.matmul(wRow, wCol)

    output_img = input_img * w

    output_img = Scaling(output_img)

    return output_img



def GaussianKernel(matrix, sigma):
    matrix = -(np.power(matrix, 2) / (2 * np.power(sigma, 2)))
    matrix = np.exp(matrix)
    matrix = matrix * (1/(2 * math.pi * np.power(sigma, 2)))

    return matrix



def SpatialGaussianComponent(n, sigma):
    spatialComponent = np.zeros((n, n), dtype= np.float64)

    mid = n // 2
    for x in range(0, n):
        for y in range(0, n):
            spatialComponent[x,y] = math.sqrt(pow(x - mid, 2) + pow(y - mid, 2))

    spatialComponent = GaussianKernel(spatialComponent, sigma)

    return spatialComponent



def RangeGaussianComponent(intensity, n, sigma):
    rangeComponent = np.zeros((n, n), dtype= np.float64)

    mid = n // 2
    for x in range(0, n):
        for y in range(0, n):
               rangeComponent[x,y] = intensity[x,y] - intensity[mid,mid]

    rangeComponent = GaussianKernel(rangeComponent, sigma)

    return rangeComponent



def Scaling(input_img):
    minI = np.amin(input_img)
    maxI = np.amax(input_img)

    output_img = (input_img - minI) * 255 / (maxI - minI) 

    return output_img



def RSE(m, r):
    r = np.float64(r)

    error = np.power(m - r, 2)
    error = np.sum(error)
    error = np.sqrt(error)

    return error




if __name__ == '__main__':
    filename = str(input()).rstrip()
    input_img = imageio.imread(filename)
    method = int(input())
    save = int(input())

    if method == 1:
        n = int(input())
        spatialSigma = float(input())
        rangeSigma = float(input())
        output_img = BilateralFilter(input_img, n, spatialSigma, rangeSigma)

    elif method == 2:
        c = float(input())
        kernel = int(input())
        output_img = LaplacianFilter(input_img, kernel, c)

    elif method == 3:
        rowSigma = float(input())
        colSigma = float(input())
        output_img = VignetteFilter(input_img, rowSigma, colSigma)

    if save == 1:
        imageio.imwrite("output_img.png",output_img)

    print("%.4f" % RSE(output_img, input_img))